// Start sticky navbar
$(function() {
    var win = $(window);
    var header = $('#ipsLayout_header');
    var height = $('header').outerHeight(true);
    
    win.on('load scroll', function() {
      if(win.scrollTop() > height) {
        header.addClass('sticky');
        header.css({
          marginTop: +height
        });
      } else {
        header.removeClass('sticky');
        header.css({
          marginTop: 0
        });
      }
    });
  });
//   End sticky navbar


// Scroll top arrow

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
   
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
 
     $('html, body').animate({scrollTop:0}, 'slow');
}

// End


//Side navigation
$(document).ready(function () {
  $("#sidebar").mCustomScrollbar({
      theme: "minimal"
  });

  $('#dismiss, .overlay').on('click', function () {
      // hide sidebar
      $('#sidebar').removeClass('active');
      // hide overlay
      $('.overlay').removeClass('active');
  });

  $('#sidebarCollapse').on('click', function () {
      // open sidebar
      $('#sidebar').addClass('active');
      // fade in the overlay
      $('.overlay').addClass('active');
      $('.collapse.in').toggleClass('in');
      $('a[aria-expanded=true]').attr('aria-expanded', 'false');
  });
});

//End






